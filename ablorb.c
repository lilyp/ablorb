/* Copyright © 2023 Liliana Prikler <liliana.prikler@gmail.com>

 * This application is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.

 * This application is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifndef G_LOG_USE_STRUCTURED
#define G_LOG_USE_STRUCTURED
#endif

#include <glib.h>
#include <glib-object.h>
#include <glib/gprintf.h>
#include <gio/gio.h>

#include <libgnomevfs/gnome-vfs-mime.h>

#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>

#include <stdbool.h> /* true, false */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

typedef struct ablorb_context_ ablorb_context;
struct ablorb_context_
{
  xmlXPathCompExpr  *xpath;
  gchar            **namespaces;
  xmlDoc            *document;
  gchar             *file_root;
  gchar             *output_file;
};

void
ablorb_print_version (FILE *f)
{
  fputs (
#ifdef ABLORB_VERSION
         "ablorb " ABLORB_VERSION
#else
         "ablorb 0"
#endif
         , f ? f : stderr);
}

void
ablorb_context_free (ablorb_context *ctx)
{
  if (!ctx) return;

  g_clear_pointer (&ctx->xpath, xmlXPathFreeCompExpr);
  g_clear_pointer (&ctx->namespaces, g_strfreev);
  g_clear_pointer (&ctx->file_root, g_free);
  g_clear_pointer (&ctx->output_file, g_free);
  g_clear_pointer (&ctx->document, xmlFreeDoc);
}

G_DEFINE_AUTOPTR_CLEANUP_FUNC (ablorb_context, ablorb_context_free)
G_DEFINE_AUTOPTR_CLEANUP_FUNC (xmlXPathContext, xmlXPathFreeContext)
G_DEFINE_AUTOPTR_CLEANUP_FUNC (xmlXPathObject, xmlXPathFreeObject)

const gchar *ablorb_default_xpath =
  "//html:a/@href|//html:img/@src|//html:link/@href|//html:script/@src"
  "|" "//svg:image/@href"
  "";

const gchar *ablorb_default_namespaces[] =
  {
    "html=http://www.w3.org/1999/xhtml",
    "svg=http://www.w3.org/2000/svg",
    NULL
  };

gboolean
file_to_data (const gchar *filename, const gchar *mime, gchar **data)
{
  gchar buffer[3060];
  gchar base64buffer[4096];
  gint base64state[2] = {0, 0};
  gssize count;
  gsize n_encoded;

  g_autoptr (GFile) file = g_file_new_for_path (filename);
  g_autoptr (GInputStream) is = G_INPUT_STREAM (g_file_read (file, NULL, NULL));
  g_autoptr (GMemoryOutputStream) encode_stream =
    G_MEMORY_OUTPUT_STREAM (g_memory_output_stream_new_resizable ());
  g_autoptr (GError) error = NULL;

  memset (base64buffer, 0, sizeof (base64buffer));

  if (!file)
    return false;

  while (count = g_input_stream_read (is, buffer, sizeof (buffer), NULL, NULL))
    {
      if (count < 0)
        return false;
      n_encoded =
        g_base64_encode_step ((guchar *)buffer, count, false, base64buffer,
                              base64state, base64state + 1);
      if (!g_output_stream_write_all (G_OUTPUT_STREAM (encode_stream),
                                      base64buffer,
                                      n_encoded,
                                      NULL, NULL, &error))
        {
          g_warning ("%s", error->message);
          return false;
        }
    }

  memset (base64buffer, 0, sizeof (base64buffer));
  n_encoded =
    g_base64_encode_close (false, base64buffer, base64state, base64state + 1);
  if (!g_output_stream_write_all (G_OUTPUT_STREAM (encode_stream),
                                  base64buffer,
                                  n_encoded + 1, /* also copy NUL byte */
                                  NULL, NULL, &error) ||
      !g_output_stream_close (G_OUTPUT_STREAM (encode_stream),
                              NULL, &error))
    {
      g_warning ("%s", error->message);
      return false;
    }

  gchar *encoded = g_memory_output_stream_get_data (encode_stream);
  g_autofree gchar *uriescaped =
    g_uri_escape_string (encoded, NULL, false);
  *data = g_strdup_printf ("data:%s;base64,%s", mime, uriescaped);
  return true;
}

static void
ablorb (ablorb_context *ctx, xmlNodePtr cur)
{
  g_autofree xmlChar *content = xmlNodeGetContent (cur->children);
  g_autoptr (GUri) uri = g_uri_parse ((gchar *)content, G_URI_FLAGS_NONE, NULL);
  g_info ("potential candidate: %s", content);
  if (uri) return; /* only interested in file names */
  g_autofree gchar *filename =
    g_build_filename (ctx->file_root, content, NULL);
  g_info ("real file name: %s", filename);
  const gchar *mime =
    /* XXX: Using suffix_only, because GVFS' handling of magic sequences
     * appears to be broken. */
    gnome_vfs_get_file_mime_type (filename, NULL, true);
  g_autofree xmlChar *new_value = NULL;

  if (file_to_data (filename, mime, (gchar **)&new_value))
    xmlNodeSetContent (cur->children, new_value);
}

static void
ablorb_file (ablorb_context *ctx, char *docname)
{
  g_assert (ctx->document == NULL);

  ctx->file_root = g_path_get_dirname (docname);
  ctx->document = xmlParseFile (docname);

  g_return_if_fail (ctx->document != NULL && "Document not parsed successfully");

  g_autoptr (xmlXPathContext) xpctx = xmlXPathNewContext (ctx->document);
  xpctx->node = xmlDocGetRootElement (ctx->document);

  for (guint i = 0; i < g_strv_length (ctx->namespaces); ++i)
    {
      g_auto (GStrv) namespace = g_strsplit (ctx->namespaces[i], "=", 2);
      g_debug ("Registering namespace %s=%s", namespace[0], namespace[1]);
      xmlXPathRegisterNs (xpctx,
                          (xmlChar *)namespace[0],
                          (xmlChar *)namespace[1]);
    }

  g_autoptr (xmlXPathObject) res = xmlXPathCompiledEval (ctx->xpath, xpctx);
  g_return_if_fail (res != NULL && res->type == XPATH_NODESET);

  gint n_matches = xmlXPathNodeSetGetLength (res->nodesetval);
  g_info ("found %d matches", n_matches);
  for (gint i = 0; i < n_matches; ++i)
    ablorb (ctx, xmlXPathNodeSetItem (res->nodesetval, i));

  xmlSaveFile (ctx->output_file ? ctx->output_file : "-", ctx->document);
}

int
main (int argc, char **argv)
{
  if (argc < 1) return EXIT_FAILURE;

  gboolean print_version = false;
  g_autofree gchar *xpath = NULL;
  g_autoptr (ablorb_context) ctx = g_new0 (ablorb_context, 1);
  g_autoptr (GError) error = NULL;

  GOptionEntry options[] =
    {
      {"xpath", 'p', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING,
       &xpath,
       "Search for file names in nodes found via XPATH", "XPATH"},
      {"namespace", 'n', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING_ARRAY,
       &ctx->namespaces,
       "Use NS as a shorthand for URI in XPATH", "NS=URI"},
      {"output", 'o', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING, &ctx->output_file,
       "Write to OUTPUT instead of stdout", "OUTPUT"},
      {"version", 'v', G_OPTION_FLAG_NONE, G_OPTION_ARG_NONE, &print_version,
       "Print version information", NULL},
      {0}
    };

  g_autoptr (GOptionContext) optctx = g_option_context_new ("FILE");
  g_option_context_add_main_entries (optctx, options, NULL);

  g_log_writer_default_set_use_stderr (true);
  if (!g_option_context_parse (optctx, &argc, &argv, &error))
    {
      g_error ("%s", error->message);
      return EXIT_FAILURE;
    }

  if (print_version)
    {
      ablorb_print_version (NULL);
      return EXIT_SUCCESS;
    }

  ctx->xpath = xmlXPathCompile ((xmlChar *)(xpath ? xpath : ablorb_default_xpath));
  if (!ctx->xpath)
    {
      g_error ("failed to compile xpath");
      return EXIT_FAILURE;
    }

  if (!ctx->namespaces)
    ctx->namespaces = g_strdupv (ablorb_default_namespaces);

  if (argc <= 1)
    {
      g_warning ("no document given");
      return EXIT_SUCCESS;
    }
  else if (argc > 2)
    {
      g_warning ("ignoring additional documents after %s", argv[1]);
    }

  ablorb_file (ctx, argv[1]);

  return EXIT_SUCCESS;
}
