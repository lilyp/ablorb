(use-modules (guix gexp)
             (guix packages)
             (guix git-download)
             (guix utils)
             (guix build-system meson)
             (gnu packages glib)
             (gnu packages gnome)
             (gnu packages pkg-config)
             (gnu packages xml)
             ((guix licenses) #:select (gpl3+)))

(define-syntax-rule (and/fn functions ...)
  (lambda args (and (apply functions args) ...)))

(define %source-dir (dirname (current-filename)))

(define ablorb
  (package
    (name "ablorb")
    (version "0.1.0+git")
    (source (local-file %source-dir
                        #:recursive? #t
                        #:select?
                        (and/fn (or (git-predicate %source-dir) (const #t))
                                (lambda (file stat)
                                  (not (string-contains file "guix.scm"))))))
    (build-system meson-build-system)
    (inputs (list glib gconf gnome-vfs libxml2))
    (native-inputs (list pkg-config))
    (home-page "https://gitlab.gnome.org/lilyp/ablorb")
    (synopsis "Replace asset links with data URIs")
    (description "Ablorb takes an XML file and resolves relative links,
replacing them with data URIs.")
    (license gpl3+)))

ablorb
