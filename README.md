# Ablorb

Don't you hate it when your carefully handcrafted HTML files reference
files on *your* disk that aren't available to others?  Well, fear no longer!
With one weird trick you can pull all of those into one giant not really
readable, but incredibly movable file!  Static website generators hate it!

## Installing

Make sure you have glib, gnome-vfs, and libxml2 installed.  (This should
be the case on most GNOME systems anyway, but you might lack their
development headers).  Then type

```sh
$ meson setup build
$ meson compile -C build
$ sudo meson install -C build
```

Alternatively, you can build this package via GNU Guix:

```sh
$ guix build -f guix.scm
```

## Usage

The basic syntax is

```sh
$ ablorb XML_FILE
```

Use `ablorb --help` for a list of available options.

## Alternatives

[Monolith](https://github.com/Y2Z/monolith) tries to do the same, but
doesn't produce well-formed XML.  Eww.

## FAQ

- *How do you pronounce ablorb?* Like absorb, but change the s to an l.
- *Why did you write this in C?  Eww.  That's so unsafe!*
  I prefer the ability to properly close XML tags over memory safety.  🙂
  Also, Rust folks hate g_autofree.
- *But what if I want to have cyclic references?*  Don't.
- *But what if I want to inline external sources?*  Don't.
  Also, consider not linking to Google, Meta, etc. in the first place.
- *Who wants to handwrite HTML in ${CURRENT_YEAR} anyway?*
  I did, that's why I wrote this.
- *But what if I want to use Markdown?*
  Push it through `cmark` or similar first, then ablorb it.
- *But what if I want to use something other than HTML/SVG?*
  We don't kink-shame you.  As long as your format can be read with libxml2
  and supports data URIs, you should be fine.  You might want to use
  different XPaths, however.
- *But what if it's not something XML-based?* Say what?
- *I found a bug, what should I do?*
  [Drop me an email](mailto:liliana.prikler@gmail.com) or
  [file an issue](https://gitlab.gnome.org/lilyp/ablorb/issues/new).
